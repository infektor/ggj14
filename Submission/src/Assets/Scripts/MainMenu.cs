﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour
{
    private static float width = 400.0f; // Width of button
    private static float height = 40.0f; // Height of button
    private static float xPos = (20.0f); // X position of first button
    private static float yPos = Screen.height / 8.0f; // Y position of first button
    private static float margin = 10.0f; // Distance between buttons
	public GUIStyle buttonStyle;
	//Funky camera
	private Camera cam;
	public GameObject target;

	float wobbl = 0;

	void Start(){
		cam = gameObject.camera;
	}

	void FixedUpdate() {
		wobbl ++;
		cam.transform.LookAt(target.transform);
		cam.transform.Translate(new Vector3 (0.05f*Mathf.Sin(wobbl / 150.0f), 0,0.05f* Mathf.Cos(wobbl /150.0f)));;
	}

	void Update(){
		//wobbl += Time.fixedDeltaTime
		//cam.transform.LookAt(target.transform);
		//cam.transform.Translate(new Vector3 (0.05f*Mathf.Sin(wobbl / 3.0f), 0,0.05f* Mathf.Cos(wobbl / 3.0f)));
		//cam.transform.Translate(new Vector3 (0.05f*Mathf.Sin(Time.frameCount / 150.0f), 0,0.05f* Mathf.Cos(Time.frameCount / 150.0f)));
	}

	void OnGUI()
	{
		buttonStyle.fontSize = 80;
		if (GUI.Button (new Rect (xPos, yPos, width, height), "FACE OFF", buttonStyle))
		{
            
		}
		buttonStyle.fontSize = 48;

		if (GUI.Button(new Rect(xPos, yPos + (height + margin) * 4, width, height), "Standard", buttonStyle))
		{
			// Load main scene
			Application.LoadLevel("S_Standard");
		}
		if (GUI.Button(new Rect(xPos, yPos + (height + margin) * 5, width, height), "Co-op", buttonStyle))
		{
			Application.LoadLevel("S_Coop");
		}
		if (GUI.Button(new Rect(xPos, yPos + (height + margin) * 6, width, height), "Fps", buttonStyle))
		{
			Application.LoadLevel("S_Fps");
		}
		if (GUI.Button(new Rect(xPos, yPos + (height + margin) * 7, width, height), "Crowd 2 Player", buttonStyle))
        {
			GM_Hunt.playerAmount = 2;
			Application.LoadLevel("HideAndSeek");
        }

		if (GUI.Button(new Rect(xPos, yPos + (height + margin) * 8, width, height), "Crowd 4 Player", buttonStyle))
        {
			GM_Hunt.playerAmount = 4;
			Application.LoadLevel("HideAndSeek");
        }
		if (GUI.Button(new Rect(xPos, yPos + (height + margin) * 9, width, height), "Quit", buttonStyle))
		{
			// Quit application
			Application.Quit();
		}
	}

}