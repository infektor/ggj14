﻿using UnityEngine;
using System.Collections;

public class GM_Fps : GameManager {

	int[] playerstate = {0, 0, 0, 0};

	public override void Awake()
	{
		base.Awake ();
	}
	
	public override void Update(){
		if (mode == modes.battleready) {
			if(isAllReady(4)){
				reset ();
			}
		}

		if (Input.GetKeyDown ("w")) {
			print ("1 kills 2");
			test (1,2);
		}
		if (Input.GetKeyDown ("e")) {
			print ("1 kills 3");
			test (1,3);;
		}
		if (Input.GetKeyDown ("r")) {
			print ("1 kills 4");
			test (1,4);
		}
		base.Update ();
	}
	
	//Set everyones eyes back to their real body
	public override void reset(){
		print("Reseting");
		mode = modes.battle;
		playerstate = new int[] {1, 1, 1, 1};
		change();
	}
	
	//Do all the camera Swapping, and animations
	void change(){
		for (int i=0; i<playerstate.Length; i++) {
			int state = playerstate[i];
			//dead or alive
			if(state == 0 && playerScripts[i].isAlive()){
				playerScripts[i].kill();
			}else if(state != 0 && !playerScripts[i].isAlive()){
				playerScripts[i].respawn();
			}
		}
		print("player 1 state is now " + playerstate[0]);
		print("player 2 state is now " + playerstate[1]);
		print("player 3 state is now " + playerstate[2]);
		print("player 4 state is now " + playerstate[3]);
	}

	public override void endGame(){
		Debug.Log("The fps game is over");
		winner = firstNonZero (playerstate);
		print (winner);
		//playerScripts[winner].setCameraPos(4);
		mode = modes.gameover;
	}
	
	//has the right person shot the right person?
	public override bool test(int shooter, int target){
		if (mode != modes.battle) {
			return false;
		}
		Debug.Log ("checking " + shooter + " " + target);
		shooter -= 1; //arrays Start a 0, but we start counting players at 1
		target -= 1;
		Debug.Log("KILL!!");
		//Succesful shot, kill
		playerstate[target] = 0;
		change ();
		//Is game over?
		if(count(playerstate,0) == 3){
			endGame();
		}
		return true;
	}

}
