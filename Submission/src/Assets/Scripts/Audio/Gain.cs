﻿using UnityEngine;
using System.Collections;

public class Gain : MonoBehaviour {
	public float gain;

	void OnAudioFilterRead(float[] data, int channels) {
		for (var i = 0; i < channels; ++i)
		{
			data[i] = data[i] * gain;
		}
	}
}
