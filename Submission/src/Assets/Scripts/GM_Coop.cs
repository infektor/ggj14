﻿using UnityEngine;
using System.Collections;

public class GM_Coop : GameManager {

	int[] playerstate = {0, 0, 0, 0};
	int[] teams = {0, 0, 0, 0};
	public Material Team1Material;
	public Material Team2Material;

	public override void Awake()
	{
		base.Awake ();
	}
	
	public override void Update(){
		if (mode == modes.battleready) {
			if(isAllReady(4)){
				reset ();
			}
		}

		if (Input.GetKeyDown ("w")) {
			print ("1 kills 2");
			test (1,2);
		}
		if (Input.GetKeyDown ("e")) {
			print ("1 kills 3");
			test (1,3);;
		}
		if (Input.GetKeyDown ("r")) {
			print ("1 kills 4");
			test (1,4);
		}
		base.Update ();
	}
	
	//Set everyones eyes back to their real body
	public override void reset(){
		mode = modes.battle;
		playerstate = new int[] {1, 2, 3, 4};
		teams = new int[]  {0, 0, 0, 0};
		randomize ();
	}
	
	//Do all the camera Swapping, and animations
	void change(){
		for (int i=0; i<playerstate.Length; i++) {
			int state = playerstate[i];
			//dead or alive
			if(state == 0 && playerScripts[i].isAlive()){
				playerScripts[i].kill();
			}else if(state != 0 && !playerScripts[i].isAlive()){
				playerScripts[i].respawn();
			}
			//who am I seeing through
			if(state != 0 && playerScripts[i].isAlive()){
				playerScripts[i].setCameraPos(state-1);
			}else if(state == 0){
				playerScripts[i].setCameraPos(i);
			}
			//What team am I on
			MeshRenderer mr = _players[i].GetComponentInChildren<MeshRenderer>();
			if(mr){
				Material [] mats = mr.materials;
				if(teams[i] == 1){
					mats[0] = Team1Material;
					mr.materials = mats;
				}else if(teams[i] == 2){
					mats[0] = Team2Material;
					mr.materials = mats;			
				}
			}

		}
		print("player 1 state is now " + playerstate[0]);
		print("player 2 state is now " + playerstate[1]);
		print("player 3 state is now " + playerstate[2]);
		print("player 4 state is now " + playerstate[3]);
		
	}
	
	void randomize(){
		switch (Random.Range(0, 2))
		{
		case 0:
			//1 pairs with 2
			//4 pairs with 3
			playerstate = new int[] {2, 1, 4, 3};
			teams = new int[]  {1, 1, 2, 2};
			break;
		case 1:
			//1 pairs with 3
			//4 pairs with 2
			playerstate = new int[] {3, 4, 1, 2};
			teams = new int[]  {1, 2, 1, 2};
			break;
		case 2:
			//1 pairs with 4
			//3 pairs with 2
			playerstate = new int[] {4, 3, 2, 1};
			teams = new int[]  {1, 2, 2, 1};
			break;
		}
		change();
	}
	
	
	public override void endGame(){
		Debug.Log("The coop game is over");
		mode = modes.gameover;
	}
	
	//has the right person shot the right person?
	public override bool test(int shooter, int target){
		if (mode != modes.battle) {
			return false;
		}
		//Debug.Log ("checking " + shooter + " " + target);
		shooter -= 1; //arrays Start a 0, but we start counting players at 1
		target -= 1;
		if (teams[shooter] != teams[target]) {
			Debug.Log("KILL!!");
			//Succesful shot, kill
			playerstate[target] = 0;
			//give eyes back
			change ();
			//
			bool team1Alive = false;
			bool team2Alive = false;
			foreach(int a in playerstate){
				//for all alive players
				if (playerstate[a] > 0){
					//which team are they on?
					if( teams[a] == 1){
						team1Alive = true;
					}else{
						team2Alive = true;
					}
				}
			}
			//is a team wiped out?
			if(!team1Alive || !team2Alive){
				if(team1Alive){
					winner = 1;
				}else if (team2Alive){
					winner = 2;
				}else{
					//TODO draw conditions
					winner = 0;
				}
				endGame();
			}
			return true;
		}
		return false;
	}
}
