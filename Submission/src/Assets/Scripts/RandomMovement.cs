﻿using UnityEngine;
using System.Collections;

public class RandomMovement : MonoBehaviour {
	float direction;

	// Use this for initialization
	void Start () {
		if (Random.Range (0, 1.0f) > 0.5f) {
			direction = -1.0f;
		} else {
			direction = 1.0f;
		}
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate(new Vector3 (direction*0.05f*Mathf.Sin(Time.frameCount / 150.0f), 0,direction* 0.05f* Mathf.Cos(Time.frameCount / 150.0f)));
		transform.Rotate (new Vector3 (0, direction*0.5f, 0));
	}
}
