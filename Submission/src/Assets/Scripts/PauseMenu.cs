﻿using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour
{
    private static float width = 100.0f; // Width of button
    private static float height = 40.0f; // Height of button
    private static float xPos = (Screen.width / 2.0f) - (width / 2.0f); // X position of first button
    private static float yPos = Screen.height / 4.0f; // Y position of first button
    private static float margin = 10.0f; // Distance between buttons

    private bool paused = false;

    void OnGUI()
    {
        if (paused)
        {
            if (GUI.Button(new Rect(xPos, yPos, width, height), "Resume Game"))
            {
                // Unpause game
                paused = false;
            }

            if (GUI.Button(new Rect(xPos, yPos + height + margin, width, height), "Options"))
            {
                print("Options");
            }

            if (GUI.Button(new Rect(xPos, yPos + (height + margin) * 2, width, height), "Main Menu"))
            {
                // Load menu scene
                Application.LoadLevel("Menu");
            }
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !paused)
        {
            // Pause game
            paused = true;
        }
    }
}