﻿using UnityEngine;
using System.Collections;

public class GM_Hunt : GameManager {


	public static int playerAmount;
	int[] playerstate;
	public GameObject playerPrefab;
	public GameObject [] spawnpoints;

	public override void Awake()
	{
		if (playerAmount == 0) {
			playerAmount = 2;
		}
		_players = new GameObject[playerAmount];
		playerstate = new int[playerAmount]; 
		playerScripts = new PlayerScript[playerAmount];
		for (int i=0; i<playerAmount; i++) {
			//instate
			_players [i] = Instantiate (playerPrefab, spawnpoints[i].transform.position, Quaternion.identity) as GameObject;
			_players [i].tag = "player"+(i+1);
			playerScripts [i] = _players[i].GetComponent<PlayerScript> ();
			playerScripts [i].playerNumber = i+1;
			playerScripts [i].setManager (this.GetComponent<GM_Hunt>());
			Debug.Log("there");
			playerScripts [i].setReady (false);
		}
		mode = modes.battleready;
	}
	void Start(){

	}

	public override void Update(){
		if (playerAmount == 2 && mode == modes.battleready) {
			playerScripts [0].setCameraPos(5);
			playerScripts [1].setCameraPos(6);
		}

		if (mode == modes.battleready) {
			if(isAllReady(playerAmount)){
				reset ();
			}
		}
		
		if (mode == modes.gameover) {
			//go back to main menu on Space or 5 seconds
			if (Input.GetKeyDown ("space") || resetTime <= 0) {
				Application.LoadLevel ("Menu");
			}
			resetTime -= Time.deltaTime;
		}

		base.Update ();
	}
	
	//Set everyones eyes back to their real body
	public override void reset(){
		print("Reseting");
		mode = modes.battle;
		for(int i=0;i<playerAmount;i++){
				playerstate[i] =1;
		}
		change();
	}
	
	//Do all the camera Swapping, and animations
	void change(){
		for (int i=0; i<playerstate.Length; i++) {
			int state = playerstate[i];
			//dead or alive
			if(state == 0 && playerScripts[i].isAlive()){
				playerScripts[i].kill();
			}else if(state != 0 && !playerScripts[i].isAlive()){
				//Why would we never need to respawn?
				//playerScripts[i].respawn();
			}
		}

		for(int i=0;i<playerAmount;i++){
		//	playerstate[i] =1;
			print("player "+i+" state is now " + playerstate[i]);
		}
	}

	public override void endGame(){
		Debug.Log("The fps game is over");
		winner = firstNonZero (playerstate);
		print (winner);
		//playerScripts[winner].setCameraPos(4);
		mode = modes.gameover;
	}
	
	//has the right person shot the right person?
	public override bool test(int shooter, int target){
		if (mode != modes.battle) {
			return false;
		}
		Debug.Log ("checking " + shooter + " " + target);
		shooter -= 1; //arrays Start a 0, but we start counting players at 1
		target -= 1;
		Debug.Log("KILL!!");
		//Succesful shot, kill
		playerstate[target] = 0;
		change ();
		//Is game over?
		int dead = count (playerstate, 0);
		if( (playerAmount == 4 && dead == 3) || (playerAmount == 2 && dead == 1)){
			endGame();
		}
		return true;
	}

}
