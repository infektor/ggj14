﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;     // Required in C#

public class FirstPersonCameraControllerXbox : MonoBehaviour 
{
    public float movementSpeed = 100.0f;
    public float rotationSpeed = 4.0f;
    private float vibeTime = 0.0f;

    GamePadState state;
    PlayerIndex playerIndex;
    int playerNumber = 0;
	PlayerScript PL;

    Rigidbody body;

	// Use this for initialization
	void Start () 
    {
		PL = this.GetComponent<PlayerScript>();

        if (this.tag == "player1")
        {
            playerIndex = PlayerIndex.One;
            playerNumber = 1;
        }
        else if (this.tag == "player2")
        {
            playerIndex = PlayerIndex.Two;
            playerNumber = 2;
        }
        else if (this.tag == "player3")
        {
            playerIndex = PlayerIndex.Three;
            playerNumber = 3;
        }
        else if (this.tag == "player4")
        {
            playerIndex = PlayerIndex.Four;
            playerNumber = 4;
        }

        body = this.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () 
    {
        state = GamePad.GetState(playerIndex);  // Get Current state of the Gamepad

        if (Input.GetAxisRaw("A_" + playerNumber) == 1)
        {
			PL.fire();
        }

        float lStickX = Input.GetAxisRaw("L_XAxis_" + playerNumber);
        float lStickY = -Input.GetAxisRaw("L_YAxis_" + playerNumber);
        float rStickX = Input.GetAxisRaw("R_XAxis_" + playerNumber);

        float lTrigger = Input.GetAxisRaw("TriggersL_" + playerNumber);
        float rTrigger = Input.GetAxisRaw("TriggersR_" + playerNumber);

        body.AddRelativeForce(new Vector3(lStickX, 0.0f, lStickY) * movementSpeed);   //TODO: Need a better way of doing this

        body.AddRelativeForce(new Vector3(0.0f, rTrigger + lTrigger, 0.0f) * movementSpeed);   //TODO: Need a better way of doing this

        body.transform.Rotate(new Vector3(0.0f, 1.0f, 0.0f), rStickX * rotationSpeed);

        if (lTrigger > 0.0f || rTrigger > 0.0f)
        {
            if (!audio.isPlaying)
            {
                audio.Play();
            }
        }
        else audio.Stop();

        if (vibeTime > 0.0f)
        {
            Vibrate(1.0f);
            vibeTime -= Time.deltaTime;
        }
        else
            Vibrate();
	}

    void Vibrate(float pow)
    {
        GamePad.SetVibration(playerIndex, pow, pow);
    }

    void Vibrate()
    {
        GamePad.SetVibration(playerIndex, 0.0f, 0.0f);
    }

    void OnTriggerEnter(Collider hit)
    {
      //  if (hit.tag == "wall")
        //{
            vibeTime = 0.4f;
        //}
    }
}
