﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour 
{
	float lifetime;
	GameObject parent;
    float bullDuration = 1.0f;
	// Use this for initialization
	void Start () 
    {
		//Debug.Log ("I am a bullet and I am alive!");
	}
	
	// Update is called once per frame
	void Update () 
    {
        lifetime += Time.deltaTime;
		transform.Translate (new Vector3 (0, 0, 80.0f) * Time.deltaTime);
		if (lifetime > 0.8f) 
        {
			kill();
		}
	}

	void OnTriggerEnter (Collider check)
	{
		if (check.gameObject == parent) 
        {
			return;
		}
		//Debug.Log ("Bullet has Collided with: "+check.gameObject.tag);
		if (check.gameObject.tag.StartsWith ("player")) 
        {
			//tell the game manager
			PlayerScript pl = check.gameObject.GetComponent<PlayerScript>();
			if(!pl.isAlive())
            {
				return;
			}
			PlayerScript paPl = parent.GetComponent<PlayerScript>();
			GameManager GM = pl.getManager();
			GM.test(paPl.playerNumber, pl.playerNumber);
			//kill myself
		}
		if (check.gameObject.tag.StartsWith ("NPC")) 
		{
			Destroy(check.gameObject);
		}
	}

	public void setParent(GameObject par)
    {
		parent = par;
	}

	void kill()
    {
		//Debug.Log ("I am a bullet and I am dead");
        parent.GetComponent<LightningBolt>().enabled = false;
        parent.GetComponent<ParticleEmitter>().enabled = false;
        parent.GetComponent<ParticleRenderer>().enabled = false;
		Destroy(gameObject);
	}
}
