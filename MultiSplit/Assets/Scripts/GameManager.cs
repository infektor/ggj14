﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
	
	protected PlayerScript[] playerScripts;
	protected GameObject[] _players;
	public enum modes {battleready,battle,gameover};
	protected modes mode;
	protected int winner;
	public float resetTime = 5.0f;
	
	public virtual void Awake()
	{
		//todo get by tag
		_players = new GameObject[] {
			GameObject.FindGameObjectsWithTag ("player1")[0],
			GameObject.FindGameObjectsWithTag ("player2")[0],
			GameObject.FindGameObjectsWithTag ("player3")[0], 
			GameObject.FindGameObjectsWithTag ("player4")[0]
		};
		playerScripts = new PlayerScript[] {
			_players[0].GetComponent<PlayerScript> (),
			_players[1].GetComponent<PlayerScript> (),
			_players[2].GetComponent<PlayerScript> (),
			_players[3].GetComponent<PlayerScript> ()
		};
		foreach (PlayerScript p in playerScripts){
			p.setManager (this);
		}
		mode = modes.battleready;
		//set all players ready to false, it goes true when they all fire
		foreach (PlayerScript player in playerScripts) {
			player.setReady(false);
		}
	}

	public virtual void Update(){
		
		if (mode == modes.gameover) {
			//go back to main menu on Space or 5 seconds
			if (Input.GetKeyDown ("space") || resetTime <= 0) {
				Application.LoadLevel ("Menu");
			}
			resetTime -= Time.deltaTime;
		}

		if (Input.GetKeyDown ("w")) {
			print ("1 kills 2");
			test (1,2);
		}
		if (Input.GetKeyDown ("e")) {
			print ("1 kills 3");
			test (1,3);;
		}
		if (Input.GetKeyDown ("r")) {
			print ("1 kills 4");
			test (1,4);
		}
		if (Input.GetKeyDown ("1")) {
			playerScripts[0].setReady(true);
		}
		if (Input.GetKeyDown ("2")) {
			playerScripts[1].setReady(true);
		}
		if (Input.GetKeyDown ("3")) {
			playerScripts[2].setReady(true);
		}
		if (Input.GetKeyDown ("4")) {
			playerScripts[3].setReady(true);
		}


	}

	public bool isAllReady(int amount){
		bool a = true;
		for (int i=0; i<amount; i++) {
			if(!playerScripts[i].isReady()){
				a = false;
			}
		}
		return a;
	}

	//Set everyones eyes back to their real body
	public virtual void reset(){
		mode = modes.battle;
	}
	

	public virtual void endGame(){
		Debug.Log("The game is over");
		mode = modes.gameover;
		//who is winner
		winner = 1;
	}
	
	//has the right person shot the right person?
	public virtual bool test(int shooter, int target){
		return true;
	}
	
	public int count(int[] a, int b){
		int counter= 0;
		foreach (int item in a)
		{
			if (item == b)
			{
				counter++;
			}
		}
		return counter;
	}
	public int firstNonZero(int[] a){
		for(int i =0;i<a.Length;i++)
		{
			if (a[i] != 0 )
			{
				return i;
			}
		}
		return 0;
	}
	
	public modes getMode(){
		return mode;
	}
	public int getWinner(){
		return winner;
	}
}
