﻿using UnityEngine;
using System.Collections;

public class PlayerGUI : MonoBehaviour
{
    public int score = 0;

    public Texture pre_ready;
    public Texture ready;
    public Texture death;
    public Texture winner;
    public Texture background;

    private PlayerScript playerScript;
    private string player;
    // Width, height and margin for GUI.Box
    private float width = 70.0f;
    private float height = 22.0f;
    private float margin = 5.0f;
	private GameManager GM;

    void Start()
    {
        playerScript = GetComponent<PlayerScript>();
		GM = playerScript.getManager ();
        string tag = transform.tag;
        player = tag.Substring(tag.Length - 1);
		//player and playerScript.playerNumber should be interchanable
    }

    void OnGUI()
    {
		if (!GM) {
			//Debug.Log ("No GM yet");
			return;
		}

        // Draw ready image
        else if (GM.getMode() == GameManager.modes.battleready && !playerScript.isReady()) {
            drawCentred(pre_ready);
        }
        else if (GM.getMode() == GameManager.modes.battleready && playerScript.isReady())
        {
            drawCentred(ready);
        }

		// Draw winner image
		if (GM.getMode() == GameManager.modes.gameover) {
			if(GM.getWinner()+1 == playerScript.playerNumber){
                drawCentred(winner);
				return;
			}
		}

        // camera.pixelRect doesn't work, so let's hard code positions
        // If player is alive draw score and player number
        if (playerScript.isAlive())
        {
            switch (player)
            {
                case "1":
                    GUI.Box(new Rect(margin, margin, width, height), "Score: " + score);
                    GUI.Box(new Rect((Screen.width / 2.0f) - width - margin, margin, width, height), "Player " + player);
                    break;
                case "2":
                    GUI.Box(new Rect((Screen.width / 2.0f) + margin, margin, width, height), "Score: " + score);
                    GUI.Box(new Rect(Screen.width - width - margin, margin, width, height), "Player " + player);
                    break;
                case "3":
                    GUI.Box(new Rect((Screen.width / 2.0f) + margin, (Screen.height / 2.0f) + margin, width, height), "Score: " + score);
                    GUI.Box(new Rect(Screen.width - width - margin, (Screen.height / 2.0f) + margin, width, height), "Player " + player);
                    break;
                case "4":
                    GUI.Box(new Rect(margin, (Screen.height / 2.0f) + margin, width, height), "Score: " + score);
                    GUI.Box(new Rect((Screen.width / 2.0f) - width - margin, (Screen.height / 2.0f) + margin, width, height), "Player " + player);
                    break;
                default:
                    break;
            }
        }
        else // Else draw death textures
        {
            drawCentred(background, death);
        }
    }

    private void drawCentred(params Texture[] textures)
    {
        // Loop through each texture and draw at centre of camera.
        foreach (Texture texture in textures)
        {
            switch (player)
            {
                case "1":
                    GUI.DrawTexture(new Rect((Screen.width / 4.0f) - (texture.width / 2.0f), (Screen.height / 4.0f) - (texture.height / 2.0f), texture.width, texture.width), texture);
                    break;
                case "2":
                    GUI.DrawTexture(new Rect(((Screen.width / 4.0f) * 3) - (texture.width / 2.0f), (Screen.height / 4.0f) - (texture.height / 2.0f), texture.width, texture.width), texture);
                    break;
                case "3":
                    GUI.DrawTexture(new Rect(((Screen.width / 4.0f) * 3) - (texture.width / 2.0f), ((Screen.height / 4.0f) * 3) - (texture.height / 2.0f), texture.width, texture.width), texture);
                    break;
                case "4":
                    GUI.DrawTexture(new Rect((Screen.width / 4.0f) - (texture.width / 2.0f), ((Screen.height / 4.0f) * 3) - (texture.height / 2.0f), texture.width, texture.width), texture);
                    break;
                default:
                    break;
            }
        }
    }
}