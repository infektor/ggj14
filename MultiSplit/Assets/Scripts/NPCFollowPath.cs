﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NPCFollowPath : MonoBehaviour 
{
    public Transform[] nodes;
    private bool[] reached;
    private int nodeTarget;

    float moveSpeed = 4.0f;
    float rotateSpeed = 4.0f;
    float offset;
    float nextNodeDistance = 8.0f;

	public Material[] materials;

    void OnDrawGizmos()
    {
        // Uncomment out to draw path in editor (must have iTween)
        //iTween.DrawPath(nodes);
        //iTween.DrawPathHandles(nodes);
    }

	// Use this for initialization
	void Start () 
    {
		int a = Random.Range(0, 5);
		if (a < 3 ) {
			MeshRenderer mr = gameObject.GetComponentInChildren<MeshRenderer>();
			if (mr) {
				Material [] mats = mr.materials;
				mats [0] = materials[a];
				mr.materials = mats;
			}
		}


        offset = Random.Range(-6.0f, 6.0f);
        moveSpeed = Random.Range(2.0f, 10.0f);
        rotateSpeed = Random.Range(2.0f, 10.0f);
        //nextNodeDistance = Random.Range(1.8f, 2.0f);

        reached = new bool[nodes.Length];
	}
	
	// Update is called once per frame
	void FixedUpdate () 
    {
        Vector3 targetPos = nodes[nodeTarget].position;
        Vector3 myPos = this.transform.position;
        targetPos.x += offset;
        targetPos.y = 0;
        myPos.y = 0;
        
        float distance = Vector3.Distance(myPos, targetPos);
        Vector3 direction = nodes[nodeTarget].position - this.transform.position;
        direction.y = 0;

        Vector3 targetDir = targetPos - transform.position;
        Vector3 forward = transform.forward;
        Vector3 localTarget = transform.InverseTransformPoint(targetPos);
 
        float angle = Mathf.Atan2(localTarget.x, localTarget.z) * Mathf.Rad2Deg;

        Vector3 eulerAngleVelocity = new Vector3(0, angle * rotateSpeed, 0);
        Quaternion deltaRotation  = Quaternion.Euler(eulerAngleVelocity * Time.deltaTime );
        rigidbody.MoveRotation(rigidbody.rotation * deltaRotation);

        direction.y -= 5.8f;

        this.rigidbody.velocity = (direction / distance) * moveSpeed;

        if (distance < nextNodeDistance)
        {
            nodeTarget++;
            if(nodeTarget == nodes.Length) 
                nodeTarget = 0;
        }
        //iTween.MoveTo(Camera.main.gameObject, iTween.Hash("path", nodes, "orientToPath", true, "lookTime", 0.2, "speed", 4.0f));
	}
}
