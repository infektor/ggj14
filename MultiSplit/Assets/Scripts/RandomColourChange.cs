﻿using UnityEngine;
using System.Collections;

public class RandomColourChange : MonoBehaviour 
{
    public Material[] materials;
	// Use this for initialization
	void Start () 
    {
        int a = Random.Range(0, 5);
        if (a < 3)
        {
            MeshRenderer mr = gameObject.GetComponentInChildren<MeshRenderer>();
            if (mr)
            {
                Material[] mats = mr.materials;
                mats[0] = materials[a];
                mr.materials = mats;
            }
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}
}
