﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour 
{
	// 1,2,3 or 4
    public int playerNumber;
	public Camera camera;
	public GameObject bulletPrefab;
	private bool alive;
	private GameManager GM;
	//Cooldown between shots
	public float coolDown;
	//Camera locations
	private Rect[] rects;
	//The Robot mesh and stuff
	private GameObject robotGo;
	//Set to true when a player fires.
	public bool ready;

	// Use this for initialization
	void Start () 
    {
		alive = true;
		camera = gameObject.GetComponentInChildren<Camera> ();
        playerNumber = tag[tag.Length - 1] - '0';
		foreach (Transform kid in gameObject.transform) 
        {
			if (kid.name == "Robot_3")
            {
				robotGo = kid.gameObject;		
			}
		}

		rects = new Rect[] 
        {
			new Rect (0, 0.5f, 0.5f, 0.5f),     //top left      // TODO: Move this stuff to a seperate general helper class
			new Rect (0.5f, 0.5f, 0.5f, 0.5f),  //top right
			new Rect (0.5f, 0, 0.5f, 0.5f),     //bottom lright
			new Rect (0, 0, 0.5f, 0.5f),        //bottom left
			new Rect (0, 0, 1.0f, 1.0f),         //fullscreen
			new Rect (0, 0, 0.5f, 1.0f),		//Split left
			new Rect (0.5f, 0, 0.5f, 1.0f),		//Split right
		};

		if(!camera)
		{
			Debug.LogError ("playerScript, no camera");
		}

		if(playerNumber == 1){
			camera.rect = rects[0]; //top left
		}
		else if(playerNumber == 2){
			camera.rect = rects[1]; //top right
		}
		else if(playerNumber == 3){
			camera.rect = rects[2]; //bottom right
		}
		else if(playerNumber == 4){
			camera.rect = rects[3]; //bottom left
		}
		else{
			Debug.LogError ("invalid player number");
		}
	}
	
	// Update is called once per frame
	void Update() 
    {
		//fire ();
		if (coolDown > 0.0f) 
        {
			coolDown -= Time.deltaTime;
		}
	}

	public void disableCamera()
    {
		camera.enabled = false;
	}

	public void enableCamera()
    {
		camera.enabled = true;
	}

	public Camera getCamera()
    {
		return camera;
	}

	public void fire()
    {
		ready = true;
		if (!isAlive() || GM.getMode() != GameManager.modes.battle) 
        {
			return;
		}
		if (coolDown <= 0.0f) 
        {
			coolDown = 2;
			Quaternion rotation = transform.rotation;
			//rotation.x += 180;
			GameObject bullet = (GameObject)Instantiate (bulletPrefab, transform.position, rotation);
			bullet.GetComponent<BulletScript> ().setParent (gameObject);

            this.gameObject.GetComponent<LightningBolt>().enabled = true;
            this.gameObject.GetComponent<LightningBolt>().UpdateTarget(bullet.transform);
            
		}
	}

	public void kill()
    {
		alive = false;
       	robotGo.SetActive(false);
		Debug.Log ("Player"+playerNumber+" is kill");
	}

	public void respawn()
    {
		Debug.Log ("Player"+playerNumber+" is alive");
		//gameObject.SetActive (true);
		robotGo.SetActive(true);
		alive = true;
	}

	public void setManager(GameManager game)
    {
		GM = game;
	}

	public GameManager getManager()
    {
		return GM;
	}

	public bool isAlive()
    {
		return alive;
	}

	public void setCameraPos(int pos)
    {
		if (pos == 4) 
        {
            camera.depth = 10;  //fullscreen so push to front
		} 
        else 
        {
			camera.depth = 3;
		}

		print ("Camera " + playerNumber + " Swap to " + pos);

		if ((pos + 1) == playerNumber) 
        {
            //We are  seeing through our own eyes, this only happens in spectator mode, so go invisible
            robotGo.SetActive(false);
		} 
        else 
        {
			robotGo.SetActive(true);
		}

		camera.rect = rects[pos];
	}
	public void setReady(bool b)
    {
		ready = b;
	}
	public bool isReady()
    {
		return ready;
	}
}

