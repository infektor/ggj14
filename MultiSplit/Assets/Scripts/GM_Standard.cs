﻿using UnityEngine;
using System.Collections;

public class GM_Standard : GameManager {

	int[] playerstate = {0, 0, 0, 0};

	public override void Awake()
	{
		base.Awake ();
	}
	
	public override void Update(){
		if (mode == modes.battleready) {
			if(isAllReady(4)){
				reset ();
			}
		}

		if (Input.GetKeyDown ("w")) {
			print ("1 kills 2");
			test (1,2);
		}
		if (Input.GetKeyDown ("e")) {
			print ("1 kills 3");
			test (1,3);;
		}
		if (Input.GetKeyDown ("r")) {
			print ("1 kills 4");
			test (1,4);
		}
		base.Update ();
	}
	
	//Set everyones eyes back to their real body
	public override void reset(){
		mode = modes.battle;
		playerstate = new int[] {1, 2, 3, 4};
		randomize ();
	}
	
	//Do all the camera Swapping, and animations
	void change(){
		for (int i=0; i<playerstate.Length; i++) {
			int state = playerstate[i];
			if(state == 0 && playerScripts[i].isAlive()){
				playerScripts[i].kill();
			}else if(state != 0 && !playerScripts[i].isAlive()){
				playerScripts[i].respawn();
			}
			if(state != 0 && playerScripts[i].isAlive()){
				playerScripts[i].setCameraPos(state-1);
			}else if(state == 0){
				playerScripts[i].setCameraPos(i);
			}
		}
		print("player 1 state is now " + playerstate[0]);
		print("player 2 state is now " + playerstate[1]);
		print("player 3 state is now " + playerstate[2]);
		print("player 4 state is now " + playerstate[3]);
		
	}
	
	void randomize(){
		switch (Random.Range(0, 2))
		{
		case 0:
			//1 Swaps with 2
			//4 swaps with 3
			playerstate = new int[] {2, 1, 4, 3};
			break;
		case 1:
			//1 Swaps with 3
			//4 swaps with 2
			playerstate = new int[] {3, 4, 1, 2};
			break;
		case 2:
			//1 Swaps with 4
			//3 swaps with 2
			playerstate = new int[] {4, 3, 2, 1};
			break;
		}
		change();
	}
	
	
	public override void endGame(){
		Debug.Log("The game is over");
		mode = modes.gameover;
		//who is winner
		winner = 10;
		for (int i=0; i<playerstate.Length; i++) {
			if(playerstate[i] != 0){
				winner = i;
			}
		}
		if (winner != 10) {
			//Expand winners screen
			//playerScripts[winner].setCameraPos(4);
			//Show text
			
		} else {
			Debug.LogError("Undetermined winner");
		}
	}
	
	//has the right person shot the right person?
	public override bool test(int shooter, int target){
		if (mode != modes.battle) {
			return false;
		}
		//Debug.Log ("checking " + shooter + " " + target);
		shooter -= 1; //arrays Start a 0, but we start counting players at 1
		if (playerstate [shooter] == target) {
			Debug.Log("KILL!!");
			//Succesful shot, kill
			playerstate[target-1] = 0;
			//give eyes back
			//unless they are aready dead (this could happen if a single bullet hit 2 people)
			if(playerstate[shooter] != 0){
				_players[shooter].GetComponent<PlayerGUI>().score ++;
				playerstate[shooter] = shooter+1;
			}
			change ();
			//are only two left?
			if(count(playerstate,0) == 2){
				print("dual");
				//yes, now they must dual
				setupdual();
				change();
			}
			//is only 1 left?
			if(count(playerstate,0) > 2){
				endGame();
			}
			
			return true;
		}
		return false;
	}
	
	void setupdual(){
		int p1 = 0;
		int p2 = 0;
		bool a = false;
		for (int i = 0; i<playerstate.Length; i++) {
			if(playerstate[i] != 0 && !a){
				a = true;
				p1 = i;
			}else if(playerstate[i] != 0 && a){
				p2 = i;
			}
		}
		playerstate [p1] = (p2 + 1);
		playerstate [p2] = (p1 + 1);
		print ("Dual between player " + (p2 + 1) + " and " + (p1 + 1));
	}
}
