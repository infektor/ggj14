﻿using UnityEngine;
using System.Collections;

public class NPCController : MonoBehaviour 
{
    float direction;
    Rigidbody body;

    public float movementSpeed = 20.0f;

	// Use this for initialization
	void Start () 
    {
        body = this.GetComponent<Rigidbody>();
        if (Random.Range(0, 1.0f) > 0.5f)
        {
            direction = -1.0f;
        }
        else
        {
            direction = 1.0f;
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        body.velocity = (new Vector3(direction * 0.05f * Mathf.Sin(Time.frameCount / 150.0f), 0, direction * 0.05f * Mathf.Cos(Time.frameCount / 150.0f))) * 40.0f;

        body.transform.Rotate(new Vector3(0.0f, 1.0f, 0.0f), direction * 0.5f);
	}
}
