﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure; // Required in C#

public class XboxDebugScript : MonoBehaviour {
	string displayText = "Here is a block of text\nlalalala\nanother line\nI could do this all day!";
	float timePassed = 0.0f;
	string currentButton;

	bool playerIndexSet = false;
	PlayerIndex playerIndex;
	GamePadState state;
	GamePadState prevState;

	void OnGUI()
	{
		GUI.Label(new Rect(0,0,Screen.width, Screen.height), displayText);

	}

	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		//Input.GetAxis ("Fire1");

		state = GamePad.GetState(playerIndex);

		timePassed += Time.deltaTime;
//		displayText = Input.GetJoystickNames ();
		displayText = "";
		for (int i = 0; i < Input.GetJoystickNames ().Length; i++) 
		{
			displayText += "\n" + Input.GetJoystickNames()[i];
		}

		GamePad.SetVibration(playerIndex, state.Triggers.Left, state.Triggers.Right);

		getAxis ();
	}

	/// <summary>
	/// Get Axis data of the joysick
	/// </summary>
	void getAxis()
	{
		displayText += "\n" + "L_XAxis_1 : " + Input.GetAxisRaw("L_XAxis_1");
		displayText += "\n" + "L_YAxis_1 : " + Input.GetAxisRaw("L_YAxis_1");
		displayText += "\n" + "R_XAxis_1 : " + Input.GetAxisRaw("R_XAxis_1");
		displayText += "\n" + "R_YAxis_1 : " + Input.GetAxisRaw("R_YAxis_1");

		displayText += "\n" + "TriggersLR_1 : " + Input.GetAxisRaw("TriggersLR_1");
		displayText += "\n" + "TriggersR_1 : " + Input.GetAxisRaw("TriggersR_1");
		displayText += "\n" + "TriggersL_1 : " + Input.GetAxisRaw("TriggersL_1");

		displayText += "\n" + "DPad_XAxis_1 : " + Input.GetAxisRaw("DPad_XAxis_1");
		displayText += "\n" + "DPad_YAxis_1 : " + Input.GetAxisRaw("DPad_YAxis_1");

		displayText += "\n" + "\n" + "X Button - X_1: " + Input.GetAxisRaw("X_1");
		displayText += "\n" + "Y Button - Y_1: " + Input.GetAxisRaw("Y_1");
		displayText += "\n" + "A Button - A_1: " + Input.GetAxisRaw("B_1");
		displayText += "\n" + "B Button - B_1: " + Input.GetAxisRaw("A_1");

		displayText += "\n" + "\n" + "Left Bumper - LB_1: " + Input.GetAxisRaw("LB_1");
		displayText += "\n" + "Right Bumper - RB_1: " + Input.GetAxisRaw("RB_1");
		displayText += "\n" + "Start Button - Start_1: " + Input.GetAxisRaw("Start_1");
		displayText += "\n" + "Back Bumper - Back_1: " + Input.GetAxisRaw("Back_1");
	
	}
	
	/// <summary>
	/// get the button data of the joystick
	/// </summary>
	/// 
	/*
	void getButton()
	{
		if(Input.GetButton("joystick button 0"))
			currentButton = "joystick button 0";
		
		if(Input.GetButton("joystick button 1"))
			currentButton = "joystick button 1";
		
		if(Input.GetButton("joystick button 2"))
			currentButton = "joystick button 2";
		
		if(Input.GetButton("joystick button 3"))
			currentButton = "joystick button 3";
		
		if(Input.GetButton("joystick button 4"))
			currentButton = "joystick button 4";
		
		if(Input.GetButton("joystick button 5"))
			currentButton = "joystick button 5";
		
		if(Input.GetButton("joystick button 6"))
			currentButton = "joystick button 6";
		
		if(Input.GetButton("joystick button 7"))
			currentButton = "joystick button 7";
		
		if(Input.GetButton("joystick button 8"))
			currentButton = "joystick button 8";
		
		if(Input.GetButton("joystick button 9"))
			currentButton = "joystick button 9";
		
		if(Input.GetButton("joystick button 10"))
			currentButton = "joystick button 10";
		
		if(Input.GetButton("joystick button 11"))
			currentButton = "joystick button 11";
		
		if(Input.GetButton("joystick button 12"))
			currentButton = "joystick button 12";
		
		if(Input.GetButton("joystick button 13"))
			currentButton = "joystick button 13";
		
		if(Input.GetButton("joystick button 14"))
			currentButton = "joystick button 14";
		
		if(Input.GetButton("joystick button 15"))
			currentButton = "joystick button 15";
		
		if(Input.GetButton("joystick button 16"))
			currentButton = "joystick button 16";
		
		if(Input.GetButton("joystick button 17"))
			currentButton = "joystick button 17";
		
		if(Input.GetButton("joystick button 18"))
			currentButton = "joystick button 18";
		
		if(Input.GetButton("joystick button 19"))
			currentButton = "joystick button 19";	   
	}
	*/
}
